//import logo from './logo.svg';
import './App.css';
import React from 'react';
import WalletConnect from './WalletConnect';
import TransactionList from './TransactionList';
import './styles.css'; // Импорт файла стилей

function App() {
  return (
    <div>
      <h1>Metamask DAI Transactions</h1>
      <WalletConnect className="Wallet-connect" /> {/* Применение класса стилей */}
      <TransactionList className="Transaction-list" /> {/* Применение класса стилей */}
    </div>
  );
}

export default App;

