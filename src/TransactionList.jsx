import React, { useState, useEffect } from 'react';
import axios from 'axios';

function TransactionList() {
  const [transactions, setTransactions] = useState([]);

  useEffect(() => {
    const fetchTransactions = async () => {
      try {
        const response = await axios.get('апи-хуйпи'); // Замените на фактический API-адрес для получения транзакций DAI
        setTransactions(response.data);
      } catch (error) {
        console.error('Не удалось получить транзакции:', error);
      }
    };

    fetchTransactions();
  }, []);

  return (
    <div>
      <h2>Живые транзакции DAI</h2>
      <ul>
        {transactions.map((transaction) => (
          <li key={transaction.hash}>
            <div>Отправитель: {transaction.sender}</div>
            <div>Получатель: {transaction.receiver}</div>
            <div>Хэш транзакции: {transaction.hash}</div>
            <div>Номер блока: {transaction.blockNumber}</div>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default TransactionList;
