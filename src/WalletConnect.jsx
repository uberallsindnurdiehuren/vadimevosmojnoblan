import React from 'react';

function WalletConnect() {
  const connectMetamask = async () => {
    if (window.ethereum) {
      try {
        await window.ethereum.request({ method: 'eth_requestAccounts' });
        // MetaMask успешно подключен
        const ethereum = window.ethereum;

        // Получение адреса текущего аккаунта MetaMask
        ethereum.request({ method: 'eth_accounts' })
          .then((accounts) => {
            const account = accounts[0];
      
            // Получение количества транзакций (nonce) для указанного аккаунта
            ethereum.request({
              method: 'eth_getTransactionCount',
              params: [account, 'latest'],
            })
              .then((count) => {
                const transactionCount = parseInt(count, 16);
      
                // Перебор всех транзакций для указанного аккаунта
                for (let i = 0; i < transactionCount; i++) {
                  // Получение хеша каждой транзакции
                  ethereum.request({
                    method: 'eth_getTransactionByBlockNumberAndIndex',
                    params: [account, i],
                  })
                    .then((transaction) => {
                      const txhash = transaction.hash;
                      console.log('Txhash:', txhash);
                      // Здесь вы можете использовать каждый txhash для вашей логики или запросов к API
                    })
                    .catch((error) => {
                      console.error('Error:', error);
                    });
                }
              })
              .catch((error) => {
                console.error('Error:', error);
              });
          })
          .catch((error) => {
            console.error('Error:', error);
          });
      } catch (error) {
        console.error('Не удалось подключить MetaMask:', error);
      }
    } else {
      console.error('MetaMask не найден');
    }
  };

  return (
    <div>
      <button onClick={connectMetamask}>Подключить MetaMask</button>
    </div>
  );
}

export default WalletConnect;